admin = db.getSiblingDB("admin")
admin.createUser(
  {
    user: "nick",
    pwd: "nick2018",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

db.getSiblingDB("admin").auth("nick", "nick2018" )

db.getSiblingDB("admin").createUser(
  {
    "user" : "replicaAdmin",
    "pwd" : "replicaAdminPassword2018",
    roles: [ { "role" : "clusterAdmin", "db" : "admin" } ]
  }
)
